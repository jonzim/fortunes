# README #

These are the fortune data files in the original format as pulled from Unix.  The fortune application will use these files to display a random quotation (or fortune) upon command.

The files themselves are text, however some fortune application may support html/css in the quote - therefore I am listing this as html since you may use tags within each quotation.  That being said... if you write a fortune application, make sure it either strips out the tag or displays html.

### What is this repository for? ###

* The fortunes require independent maintenance from the application (or applications), therefore they require their own repository.
* The fortune files themselves are always freely available - whereas fortune readers may or may not be (depending on the developer).
* Fortune applications may be written in a variety of different languages (C, C#, Node.js, etc...) but they all must read the same fortune file format.

### How do I get set up? ###

* Download the files to the directory of your choice.
* Configuration your reader to point to your directory containing these files.
* Tell your fortune application to re-build the dat files before first run.  Some fortune applications may format the dat differently (binary vs JSON).
* Enjoy!

### Contribution guidelines ###

* Don't delete fortunes unless they are duplicates
* Don't add a fortune unless it is witty.
* If a fortune is deemed "Offensive", just move that fortune to the appropriate file in the "off" folder.
* I'm embarrassed I have to say this, but don't be easily offended.  Part of humor is to offend/be offended (tastefully).
> REPARTEE, n.  Prudent insult in retort.  Practiced by gentlemen with a constitutional aversion to violence, but a strong disposition to offend.
> 					-- The Devil's Dictionary
* Check your spelling, make sure the quote doesn't already exist in the repository, and make sure any quotations are attributed to the correct personality.
